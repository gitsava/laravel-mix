# Setting up Bootstrap 5 Workflow using Laravel Mix (Webpack)
1. Create Project
Create a blank directory for the project you are creating, I am naming my directory my-bootstrap-project.

2. Initialize NPM Project
We’ll be using NPM for the package management. Make sure you have NodeJs and NPM installed in your system. Navigate inside your newly created directory and run the following command.
```
npm init -y
```

3. Install Bootstrap
Next Up, We’ll install Bootstrap library in the project as a node dependency.
```
npm install bootstrap @popperjs/core --save-dev
```

4. Install Laravel Mix as Node Dependency
Now, Lets install Laravel Mix as node dependency by running the following command
```
npm install laravel-mix --save-dev
touch webpack.mix.js
```
```js
// webpack.mix.js

let mix = require('laravel-mix');

mix.js('src/app.js', 'dist/').sass('src/app.scss', 'dist/');
```
5. Import Bootstrap JS and SCSS
Create a new directory named src and create two blank files inside it named app.js and app.scss
```js
// src/app.js
import 'bootstrap';
```
```scss
@import "~bootstrap/scss/bootstrap";
```

6. Compile
```
npx mix
```